package g30126.nedelea.luiza.l9.e2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import g30126.nedelea.luiza.l9.e1.ButtonAndTextField;
import g30126.nedelea.luiza.l9.e1.ButtonAndTextField2;

public class Buton extends JFrame {

	 JTextField tUser;
	 JButton contor;
	 JTextArea tArea;
	Buton(){	 
        setTitle("Contorizare numar");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400,150);
        setVisible(true);
  }
	public void init(){
		 
        this.setLayout(null);
        int width=150;int height = 20;
       
       tUser = new JTextField();
        tUser.setBounds(200,50,width, height);
        add(tUser);
        
        contor = new JButton("CONTORIZARE");
        contor.setBounds(10,50,width, height);
        add(contor);
        
        tArea = new JTextArea();
        tArea.setBounds(10,180,width, height);
        add(tArea);
        contor.addActionListener(new TratareButon());
      
          }
	
	class TratareButon implements ActionListener{
		private int clicks;
		@Override
		public void actionPerformed(ActionEvent e) {
		
			String contorizare = Buton.this.tUser.getText();
			clicks++;
			Buton.this.tUser.setText(""+clicks);
			
		}
		
	}
	
	public static void main(String[] args) {
        new Buton();

  }

}
