package g30126.nedelea.luiza.l10.e3;

import g30126.nedelea.luiza.l10.e1.Counter;

public class Counters extends Thread {
	 Thread t;
	 int first;
 
	    Counters(String name, Thread t,int first){
	          super(name);
	          this.t=t;
	          this.first=first;
	          
	    }

	    public void run(){
	    	if (t!=null)
				try {
					t.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				try {
				if(first==1) {	
					for(int i=0;i<100;i++){
	                System.out.println(getName() + " i = "+i);
	                Thread.sleep((int)(Math.random() * 100));
					}
					System.out.println(getName() + " job finalised.");
				}
					else {
						for(int i=100;i<200;i++){
	                System.out.println(getName() + " i = "+i);
	                Thread.sleep((int)(Math.random() * 100));	
	                }
					System.out.println(getName() + " job finalised.");               
					} 	
				}catch (InterruptedException e1) {
					e1.printStackTrace();
				}
	    		    }

	    
	    public static void main(String[] args) {
	          Counters c1 = new Counters("counter1",null,1);
	          Counters c2 = new Counters("counter2",c1,2);
	          c1.start();
	          c2.start();
	          
}
}
