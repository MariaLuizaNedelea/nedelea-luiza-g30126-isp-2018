package Test;


public class Test {
	public static int solution(int[] A){
		int result=0;
		for(int i=0;i<7;i++)
        	for(int j=1;j<7;j++)
        			if(A[i]==A[j] && i<j)
        				result++;
		return result;
    }

    public static void main(String[] args) {
        int[] A = new int[7];
        A[0] = 9;  A[1] = 3;  A[2] = 9;
        A[3] = 3;  A[4] = 9;  A[5] = 7;
        A[6] = 9;
            int result = solution(A);

        if(result==7)
            System.out.println("Rezultat corect.");
        else
            System.out.println("Rezultat incorect.");
    }
}
