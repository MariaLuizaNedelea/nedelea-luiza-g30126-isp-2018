package g30126.nedelea.luiza.l6.e4;

import java.util.Arrays;

public class Ex4 implements CharSequence {
    private char[] s;
    public Ex4(char[] s) {
    	this.s=s;
    }

	
	public char charAt(int index) {
		return s[index];
}
	
	public int length() {
			return s.length;
			}
	
	public CharSequence subSequence(int start, int end) {
		if(start<0) start=0;
		if(end>s.length) end=s.length;
		String sub=new String(Arrays.copyOfRange(s,start,end));
		return sub;
		
	}
	
	 public String toString() {
		return s.toString();
	 }
	 
	 public static void main(String[] args) {
	        Ex4 s =new Ex4("Potato potato, big fat potato!!".toCharArray());
	       // for (int i = 0; i < s.length(); i++) {
	            System.out.print(s.charAt(0)+"\n"); //pt caraccterul de pe pozitia "0"
	       // }
	            System.out.print(s.length()+"\n");    
	        int start=7;
	        int end=14;
	        System.out.println(s.subSequence(start, end));//pt subSequence
	        

	 }
}
