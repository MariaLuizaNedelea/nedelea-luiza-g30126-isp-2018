package g30126.nedelea.luiza.l6.e4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestEx4 {
	
	@Test
		public void Test(){
		 Ex4 s =new Ex4("Potato potato, big fat potato!!".toCharArray());
		 int start=7;
	        int end=14;
	        assertEquals('P',s.charAt(0));
	        assertEquals(31,s.length());
	        assertEquals("potato,",s.subSequence(start, end));
	        
	}
		
}
