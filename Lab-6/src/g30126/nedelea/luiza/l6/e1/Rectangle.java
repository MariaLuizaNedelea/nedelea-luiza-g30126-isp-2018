package g30126.nedelea.luiza.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;

    public Rectangle(Color color, int length,int x, int y,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        if(fill==false)
        {
        g.setColor(getColor());
        g.drawRect( x, y, length, length); // 100 pe x; 150 pe y;
        }
        else {
        	g.setColor(getColor());
        	g.drawRect( x, y, length, length); // 100 pe x; 150 pe y;
        	g.fillRect(x, y, length, length);	
        	}
        }
    }
