package g30126.nedelea.luiza.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int radius,int x, int y,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
       if(fill==false)
    	   {g.setColor(getColor());
    	   g.drawOval(x,y,radius,radius);
    	   }
        else {
       	g.setColor(getColor());
       	g.drawOval( x, y, radius, radius); // 100 pe x; 150 pe y;
       	g.fillOval(x, y, radius, radius);	
       	}
    }
}
