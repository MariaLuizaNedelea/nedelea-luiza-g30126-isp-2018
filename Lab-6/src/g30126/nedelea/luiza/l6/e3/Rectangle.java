package g30126.nedelea.luiza.l6.e3;

import java.awt.*;

 class Rectangle implements Shape{
	
	 
	    private Color color;
	    int x, y;
	    final String id;
	    boolean fill;
	    private int length;

    public Rectangle(Color color, int length,int x, int y,String id,boolean fill) {
    	this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
        this.length = length;
    }

    public String getId() {
		return id;
	}
    
    public Color getColor() {
        return color;
    }
 
    public void setColor(Color color) {
        this.color = color;
    }
    
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangel "+length+" "+getColor().toString());
        if(fill==false)
        {
        g.setColor(getColor());
        g.drawRect( x, y, length, length); // 100 pe x; 150 pe y;
        }
        else {
        	g.setColor(getColor());
        	g.drawRect( x, y, length, length); // 100 pe x; 150 pe y;
        	g.fillRect(x, y, length, length);	
        	}
        }
    }
