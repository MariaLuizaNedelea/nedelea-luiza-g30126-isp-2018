package g30126.nedelea.luiza.l6.e3;

import java.awt.Graphics;

public interface Shape {
	 public String getId();
	 public void draw(Graphics g);
}
