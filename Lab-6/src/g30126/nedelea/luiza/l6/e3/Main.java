package g30126.nedelea.luiza.l6.e3;

import java.awt.*;

/**
 * @author mihai.hulea
 */
public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,20,200,"1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,40,100,"2",false);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.BLUE, 100,100,200,"3",true);
        b1.addShape(s3);
        b1.deleteById("3");
    }
}
