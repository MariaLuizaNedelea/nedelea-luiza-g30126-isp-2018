package g30126.nedelea.luiza.l6.e3;

import java.awt.*;

 class Circle implements Shape{

    private int radius;
    private Color color;
    int x, y;
    final String id;
    boolean fill;

    public Circle(Color color, int radius,int x, int y,String id,boolean fill) {
    	this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }
    
    public String getId() {
		return id;
	}
    
    public Color getColor() {
        return color;
    }
 
    public void setColor(Color color) {
        this.color = color;
    }

    
    public void draw(Graphics g) {
        System.out.println("Drawing a circle "+this.radius+" "+getColor().toString());
       if(fill==false)
    	   {g.setColor(getColor());
    	   g.drawOval(x,y,radius,radius);
    	   }
        else {
       	g.setColor(getColor());
       	g.drawOval( x, y, radius, radius); // 100 pe x; 150 pe y;
       	g.fillOval(x, y, radius, radius);	
       	}
    }
}
