package g30126.nedelea.luiza.l6.e5;

import java.awt.*;

public class Rectangle extends Shape{

    private int width;
    private int height;

    public Rectangle(Color color, int width,int height,int x, int y,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.width = width;
        this.height=height;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a rectangle[width][height] "+width+" "+height+" "+getColor().toString());
        if(fill==false)
        {
        g.setColor(getColor());
        g.drawRect( x, y, width, height); // 100 pe x; 150 pe y;
        }
        else {
        	g.setColor(getColor());
        	g.drawRect( x, y, width, height); // 100 pe x; 150 pe y;
        	g.fillRect(x, y, width, height);	
        	}
        }

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
    }
