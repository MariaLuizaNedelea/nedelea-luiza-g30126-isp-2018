package g30126.nedelea.luiza.l6.e5;

import java.awt.*;

public abstract class Shape {
 
    private Color color;
    int x, y;
    final String id;
    boolean fill;
 
    public Shape(Color color, int x, int y, String id,boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }
 
    public String getId() {
		return id;
	}

	public Color getColor() {
        return color;
    }
 
    public void setColor(Color color) {
        this.color = color;
    }
 
    public abstract void draw(Graphics g);
}
