package g30126.nedelea.luiza.l4.e5; 

import g30126.nedelea.luiza.l4.e4.Author;

public class Book {

	private String name;
	private Author author;
	private double price;
	private int qtyInStock;
	
	public Book(String name, Author author,double price)
	{
		this.name=name;
		this.author=author;
		this.price=price;
	}
	
	public Book(String name, Author author,double price,int qtyInStock)
	{
		this.name=name;
		this.author=author;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQtyInStock() {
		return qtyInStock;
	}

	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}

	public String getName() {
		return name;
	}

	public Author getAuthor() {
		return author;
	}
	
	public String toString()
	{
		return "Book name: "+name+" by "+author;
	}
	
	public String getAuthorName()
	{
		return this.author.getName();
	}
	
	public String getAuthorEmail()
	{
		return this.author.getEmail();
	}
	
	
	public static void main(String[]args)
	{	Author autor= new Author("Irina Binder","ibinder@yahoo.com",'f');
		Book carte=new Book("Insomnii",autor,45.99,143);
		System.out.println(carte);
	}
}
