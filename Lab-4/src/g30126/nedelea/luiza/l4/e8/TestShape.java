package g30126.nedelea.luiza.l4.e8;

public class TestShape {
	public static void main(String[] args){
		Shape forma=new Shape("blue",true);
		Circle cerc=new Circle(2.0,"red",false);
		Rectangle dreptunghi=new Rectangle(1.0,3.0,"black",true);
		Square patrat=new Square(4.0,"green",true);
		System.out.println(forma.toString());
		System.out.println("Aria cercului = "+cerc.getArea());
		System.out.println("Perimetrul dreptunghiului = "+dreptunghi.getPerimeter());
		System.out.println("\n");
		System.out.println(patrat.toString());
		System.out.println(dreptunghi.toString());
		System.out.println(cerc.toString());
		
	}
}
