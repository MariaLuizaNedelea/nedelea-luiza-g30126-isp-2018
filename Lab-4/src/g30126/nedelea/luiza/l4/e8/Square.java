package g30126.nedelea.luiza.l4.e8;

public class Square extends Rectangle{
	double side;
	public Square()
	{
		super();
	}
	public Square(double side)
	{
		super(side, side);	
	}
	
	public Square(double side, String color, boolean filled)
	{
	super(side,side,color,filled);	
	}
	
	public double getSide()
		{
		return super.getWidth();	
		}
	
	public void setSide(double side)
		{
		super.setLength(side);
	     super.setWidth(side);
		}
	@Override
	public void setWidth(double side){
    	super.setWidth(side);
    }
    @Override
    public void setLength(double side){
    	super.setLength(side);
    }
	public double getArea()
	   {
	      return getSide()*getSide();
	   }

	   public double getPerimeter()
	   {
	       return 4*getSide();
	    }
	   
	@Override
	public String toString() 
	{
	return "A square with side = "+getSide()+" which is a subclass of "+super.toString();
	}
}
