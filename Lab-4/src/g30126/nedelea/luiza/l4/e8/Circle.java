package g30126.nedelea.luiza.l4.e8;

public class Circle extends Shape {
	private double radius;
	
	public Circle()
	{
		radius=1.0;
	}
	
	public Circle(double radius)
	{
		this.radius=radius;
	}
	
	public Circle(double radius, String color,boolean filled)
		{
		super(color,filled);
		this.radius=radius;
		}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getArea()
		{
		return 3.14*radius*radius;
		}
	
	public double getPerimetre()
	{
		return 2*radius;
	}

	@Override
	public String toString() 
	{
	return "A circle with radius = "+radius+" which is a subclass of "+super.toString();
	}
	
	public static void main(String[]args)
	{	Shape forma=new Shape();
		Circle cerc=new Circle();
		System.out.println(forma.toString());
		System.out.println(cerc.toString());
	}
}
