package g30126.nedelea.luiza.l4.e8;

public class Shape {
	private String color;
	private boolean filled;
		public Shape()
			 {
			filled=true;
			color="red";
			 }
		public Shape(String color, boolean filled)
				{ 
				this.filled=filled;
				this.color=color;
				}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public boolean isFilled() {
			return filled;
		}
		public void setFilled(boolean filled) {
			this.filled = filled;
		}
		
	public String toString()
		{
		return "A shape of color "+color+" and filled "+filled;
		}
}
