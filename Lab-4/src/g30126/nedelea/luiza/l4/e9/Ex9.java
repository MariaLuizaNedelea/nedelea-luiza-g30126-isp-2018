package g30126.nedelea.luiza.l4.e9;

import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Wall;
import becker.robots.*;

public class Ex9 {

	public static void main(String[] args)
	   {
	      // Set up the initial situation
	      City ny = new City();
	      Robot cartof = new Robot(ny, 2, 1, Direction.NORTH);
	      
	      Wall blockAve21 = new Wall(ny, 3, 1, Direction.NORTH);
	      Wall blockAve0 = new Wall(ny, 3, 0, Direction.EAST);
	      Wall blockAve01 = new Wall(ny, 4, 0, Direction.EAST);
	      Wall blockAve011 = new Wall(ny, 5, 0, Direction.EAST);
	      Wall blockAve1 = new Wall(ny, 5, 1, Direction.WEST);
	      Wall blockAve4 = new Wall(ny, 5, 1, Direction.SOUTH);
	      Wall blockAve41 = new Wall(ny, 5, 2, Direction.SOUTH);
	      Wall blockAve11 = new Wall(ny, 5, 2, Direction.EAST);
	      
	      
	   
	      cartof.move(); 
	      cartof.turnLeft();
	      cartof.turnLeft();
	      cartof.turnLeft();
	      cartof.move();
	      cartof.turnLeft();
	      cartof.turnLeft();
	      cartof.turnLeft();
	      cartof.move();
	      cartof.move();
	      cartof.move();
	      cartof.move();
	      cartof.turnLeft();
	      cartof.turnLeft();
	   
	     
	   }
	} 
