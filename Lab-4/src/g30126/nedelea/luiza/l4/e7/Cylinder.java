package g30126.nedelea.luiza.l4.e7;

public class Cylinder extends Circle{

	private double height;
	Cylinder()
	{
		super();
		height=2.0;
	}
	
	Cylinder(double radius)
	{ 
		super(radius);
	}
	
	Cylinder(double radius,double height)
	{ 
		super(radius);
		this.height=height;
	}

	public double getHeight() {
		return height;
	}


	public double getVolume() {
		return getArea()*height;
	}
	
	public double getArea() {
		return (getRadius()+height)*2*3.14*getRadius();
	}
	
}
