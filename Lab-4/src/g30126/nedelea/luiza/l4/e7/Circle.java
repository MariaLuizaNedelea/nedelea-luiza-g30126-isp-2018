package g30126.nedelea.luiza.l4.e7;


public class Circle {

	private double radius;
	private String color="red";
	
	public Circle()
	{radius=1.0;}
	
	public Circle(double radius)
		{this.radius=radius;}
	
public double getRadius()
	{
	return radius;	
	}

public double getArea()
	{
	return radius*radius*3.14;	
	}
	
	
	public static void main(String[] args) {
		Circle ob1=new Circle();
		System.out.println(ob1.getRadius());
		System.out.println(ob1.getArea());
		

	}

}
