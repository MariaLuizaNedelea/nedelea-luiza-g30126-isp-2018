package g30126.nedelea.luiza.l4.e7;

public class TestCylinder {
	static public void main(String[]args)
	{
		Cylinder cilindru=new Cylinder();
		System.out.println("Caracteristicile cilindrului:\n"
						+"radius = "+cilindru.getRadius()
						+"\ninaltimea = "+cilindru.getHeight()
						+"\naria = "+cilindru.getArea()
						+"\nvolumul = "+cilindru.getVolume());
}
}