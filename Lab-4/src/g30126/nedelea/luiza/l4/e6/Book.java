package g30126.nedelea.luiza.l4.e6;

import g30126.nedelea.luiza.l4.e4.Author;


public class Book {

	private String name;
	private double price;
	private int qtyInStock;
	private Author [] authors = new Author[3];

	
	public Book(String name, Author[] authors,double price)
	{
		this.name=name;
		this.authors=authors;
		this.price=price;
	}
	
	public Book(String name, Author[] authors,double price,int qtyInStock)
	{
		this.name=name;
		this.authors=authors;
		this.price=price;
		this.qtyInStock=qtyInStock;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getQtyInStock() {
		return qtyInStock;
	}

	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}

	public String getName() {
		return this.name;
	}

	public Author[] getAuthors() {
		return this.authors;
	}
	
	public String toString()
	{
		return "Book name: "+this.name+" by "+this.getAuthors().length+" authors";
	}
	
	public void printAuthors()
	{
		for(int i=0;i<this.getAuthors().length;i++)
		{
			if(this.getAuthors()[i]!=null) System.out.println(this.getAuthors()[i]+" ");
		}
	}
	
	
	
	public static void main(String[]args)
	{	Author[] authors= new Author[3];
	authors[0] = new Author("Cassandra Clare", "cclare@yahoo.com", 'f');
	authors[1] = new Author("Irina Binder", "ibinder@yahoo.com", 'f');
	authors[2] = new Author("Stephen King", "sking@yahoo.com", 'm');
		Book carte=new Book("Insomnii",authors,45.99,143);
		System.out.println(carte);
		System.out.print("The authors are: ");
		carte.printAuthors();
		
	}
}
