package g30126.nedelea.luiza.l4.e4;

public class Author {
private String name;
private String email;
private char gender;

public Author(String name, String email,char gender) {
	this.name=name;
	this.email=email;
	this.gender=gender;
}

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}


public String toString() {
	return "Author- "+name+"("+gender+") "+email;
}
public static void main(String[]args)
	
{ Author ob1= new Author("Irina Binder","ibinder@yahoo.com",'f');
	String a=ob1.toString();
	System.out.println(a);
	}
}
