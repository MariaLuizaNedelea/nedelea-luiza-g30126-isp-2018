package g30126.nedelea.luiza.l2.e6;

import java.util.Scanner;

public class Ex6 {
	static void f1 (int x)
		{
		int factorial=1;
	     for(int i=1;i<=x;i++)	 factorial=factorial*i;
	     System.out.println(factorial);
		
		}
	
	static int f2 (int x)
		{
		int rezultat;

	       if(x==1)
	    	   return 1;
	       else
	       rezultat=f2(x-1)*x;
		return rezultat;
	    }

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
	      int x = in.nextInt();
	      System.out.println("Rezultatul factorialului nerecursiv:");
	      f1(x);
	      System.out.println("Rezultatul factorialului recursiv:");
	      int factorial=f2(x);
	      System.out.println(factorial);
	      
	}

}
