package g30126.nedelea.luiza.l2.e4;

import java.util.Scanner;

public class Ex4 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
	      int x = in.nextInt();
	      int[] vector;
	      vector = new int[x];
	      int i;
	      for(i=0;i<x;i++)
	    	  	vector[i]=in.nextInt();
	      int max = vector[0];
	       for(i=0;i<x;i++)
	    	   	if (max<vector[i]) max=vector[i];
	      System.out.println(max);
}
}