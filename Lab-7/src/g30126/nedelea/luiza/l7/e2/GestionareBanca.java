package g30126.nedelea.luiza.l7.e2;


public class GestionareBanca {


	public static void main(String[] args) {
	Bank banca=new Bank();
	
	banca.addAccount("User1", 200.00);
	banca.addAccount("User2", 200.00);
	banca.addAccount("User1", 800.00);
	banca.addAccount("User3", 400.00);
	banca.addAccount("User4", 500.00);
	banca.addAccount("User5", 600.00);
	banca.addAccount("User6", 550.00);
	banca.addAccount("User7", 450.00);
	
	System.out.println("Afisare dupa balanta: \n");
	banca.printAccounts();
	System.out.println("\n");
	System.out.println("\n");
	System.out.println("Afisare intre min-max: \n");
	banca.printAccounts(400.00, 700.00);
	 
}
}

	
