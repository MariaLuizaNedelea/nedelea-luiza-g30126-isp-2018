package g30126.nedelea.luiza.l7.e2;

public class  BankAccount implements Comparable<BankAccount> {

		private String owner;
		private double balance;
		BankAccount(String owner, double balance)
		{
			this.owner=owner;
			this.balance=balance;
		}
		
		public String getOwner() {
			return owner;
		}
		
		public double getBalance() {
			return balance;
		}
		
		public void widthdraw(double amount)
		{
			this.balance=this.balance-amount;
			System.out.println("Balanta contului s-a modificat cu balance-amount= "+balance);
		}
		
		public void deposit(double amount)
		{
			this.balance=this.balance+amount;
			System.out.println("S-a mai depozitat in cont cu valoarea amount= "+balance);
		}
		
		@Override
		public boolean equals(Object obj) { // se poate face si cand si owner-ul si balance-ul sunt egale;
			if(obj instanceof BankAccount){
				BankAccount p = (BankAccount)obj;
				return owner == p.owner;
			}
			return false;
		}
		
		@Override
		 public int hashCode(){
		        return owner.hashCode();
		    }
		 
		 public String toString(){
		        return "Owner: "+owner+"  Balance: "+balance;
		    }
		 
		 public int compareTo(BankAccount chestie)
		 {
			 if (this.getBalance() < chestie.getBalance())
		        {
		            return -1;
		        }

		        if (this.getBalance() > chestie.getBalance())
		        {
		            return 1;
		        }

		        return 0;
		    }


		 public static void main(String[] args) {
			 BankAccount b1= new  BankAccount("User1",200.00);
			 BankAccount b2= new  BankAccount("User1",200.00);
			 BankAccount b3= new  BankAccount("User2",200.00);
			 BankAccount b4= new  BankAccount("User3",400.00);
			 
			 if(b1.equals(b2))
					System.out.println(b1+" and "+b2+ " are equals");
				else
					System.out.println(b1+" and "+b2+ " are NOT equals");
			 
			 if(b3.equals(b4))
					System.out.println(b3+" and "+b4+ " are equals");
				else
					System.out.println(b3+" and "+b4+ " are NOT equals");

		 }


		

}
