package g30126.nedelea.luiza.l7.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;



public class Bank {

	static ArrayList<BankAccount> conturi = new ArrayList<>(); 
	 
	public ArrayList<BankAccount> getConturi() {
		return conturi;
	}


	public void addAccount(String nume, double balanta)
	 {
		 BankAccount cont= new BankAccount(nume, balanta);
		 this.conturi.add(cont);
	 }
	 
	 
	 public void printAccounts() 
	 		{  // sortate dupa balanta
			Collections.sort(conturi);
			Iterator<BankAccount> i = conturi.iterator();
	        while(i.hasNext()){
	        	BankAccount cont_actual = i.next();
	        	System.out.println(cont_actual);
	       
	        }
	 }
	 
	 public void printAccounts(double min, double max) 
		{  // afisare in range
		Iterator<BankAccount> i = conturi.iterator();
     while(i.hasNext()){
     	BankAccount cont_actual = i.next();
     	if(cont_actual.getBalance()>min &&cont_actual.getBalance()<max)
     	System.out.println(cont_actual);
    
     }
}
	 
	 
}
