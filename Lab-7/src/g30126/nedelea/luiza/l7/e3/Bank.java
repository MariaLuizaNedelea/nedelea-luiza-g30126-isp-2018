package g30126.nedelea.luiza.l7.e3;


import java.util.Iterator;
import java.util.TreeSet;



public class Bank {

	 TreeSet<BankAccount> conturi = new TreeSet<>();
	 
	 public TreeSet<BankAccount> getConturi() {
		return this.conturi;
	}


	public void addAccount(String nume, double balanta)
	 {
		 BankAccount cont= new BankAccount(nume, balanta);
		 this.conturi.add(cont);
	 }
	 
	 
	 public void printAccounts() 
	 		{  // sortate dupa balanta
			Iterator<BankAccount> i = conturi.iterator();
	        while(i.hasNext()){
	        	BankAccount cont_actual = i.next();
	        	System.out.println(cont_actual);
	       
	        }
	 }
	 
	 public void printAccounts(double min, double max) 
		{  // afisare in range
		Iterator<BankAccount> i = conturi.iterator();
     while(i.hasNext()){
     	BankAccount cont_actual = i.next();
     	if(cont_actual.getBalance()>min &&cont_actual.getBalance()<max)
     	System.out.println(cont_actual);
    
     }
}
	 
	 
}
