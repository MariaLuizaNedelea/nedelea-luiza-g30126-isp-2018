package g30126.nedelea.luiza.l7.e3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;




public class TestClass extends Bank implements Comparator<BankAccount>{
	
	@Override
	public int compare(BankAccount chestie1,BankAccount chestie2)
		{
		return chestie1.getOwner().compareToIgnoreCase(chestie2.getOwner());
		}

	public static void getAllAccounts(Bank banca) 
		{
		ArrayList<BankAccount> z1 = new ArrayList<>();
		z1.addAll(banca.getConturi());
		Collections.sort(z1, new TestClass());
		//Collections.sort(conturi, new TestClass()); //pt static
		
		 System.out.println("Ordonare alfabetica dupa Owner: ");
		for(BankAccount o:z1){
            System.out.println(o);
		}
		  
		//Iterator<BankAccount> i = conturi.iterator();  
	   //     while(i.hasNext()){
	   //     	BankAccount cont_actual = i.next();
	   //     	System.out.println(cont_actual);
	   //     } 
	}
	
	public static void main(String[] args) {
	Bank banca=new Bank();
		
		banca.addAccount("User1", 200.00);
		banca.addAccount("User2", 200.00);
		banca.addAccount("User1", 800.00);
		banca.addAccount("User3", 400.00);
		banca.addAccount("User4", 500.00);
		banca.addAccount("User5", 600.00);
		banca.addAccount("User6", 550.00);
		banca.addAccount("User7", 450.00);	
		
		getAllAccounts(banca);
		
		System.out.println("\nOrdonare dupa balanta: ");
		banca.printAccounts();
		
		System.out.println("\nOrdonare dupa range: ");
		banca.printAccounts(500.00, 700.00);

	}
}
