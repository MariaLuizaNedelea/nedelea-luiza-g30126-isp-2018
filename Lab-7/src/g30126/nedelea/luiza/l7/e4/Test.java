package g30126.nedelea.luiza.l7.e4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Test {
 public static void main(String[]args) throws IOException {
	 Dictionary dict = new Dictionary();
     char raspuns;
     String linie, explic;
     BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

     do {
        System.out.println("Meniu");
        System.out.println("a - Adauga cuvant");
        System.out.println("c - Afiseaza cuvintele din dictionar");
        System.out.println("l - Listeaza dictionar");
        System.out.println("e - Iesi");

        linie = fluxIn.readLine();
        raspuns = linie.charAt(0);

        switch(raspuns) {
           case 'a': case 'A':
              System.out.println("Introduceti cuvantul:");
              linie = fluxIn.readLine();
              Word cuvant = new Word(linie);
              if( linie.length()>1) {
                 System.out.println("Introduceti definitia:");
                 explic = fluxIn.readLine();
                 Definition def= new Definition(explic); 
                 dict.addWord(cuvant, def);
              }
           break;
           case 'c': case 'C':
              System.out.println("Afiseaza cuvinte:");
                 dict.getAllWords();

           break;
           case 'l': case 'L':
              System.out.println("Afiseaza toate definitiie:");
              dict.getAllDefinitions();
           break;

        }
     } while(raspuns!='e' && raspuns!='E');
     System.out.println("Program terminat.");

 }
}
