package g30126.nedelea.luiza.l7.e4;
import java.util.*;


public class Definition {
private String description ;

    public Definition(String description) {
     this.description=description;
    }

	public String getDescription() {
		return description;
	}
	
	 public String toString(){
	        return "Definitia: "+description;
	    }

}