package g30126.nedelea.luiza.l7.e4;
import java.util.*;

public class Word {

private String name ;
    public Word(String name) {
    	this.name=name;
    }
  
    public String getName() {
		return name;
	}    
    
    public String toString(){
        return "Cuvant: "+name;
    }
}