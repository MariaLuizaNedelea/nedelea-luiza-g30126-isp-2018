package g30126.nedelea.luiza.l3.e6;

import java.util.Scanner;


public class MyPoint {
int x,y;
public MyPoint()
		{x=0;
		 y=0;
		}

public MyPoint(int x, int y)
				{this.x=x;
				 this.y=y;
				}

public int getX() {
	return x;
	}
public void setX(int x) {
	this.x = x;
	}
public int getY() {
	return y;
	}
public void setY(int y) {
	this.y = y;
	}

public String toString() {
	String rezultat="("+Integer.toString(x)+","+Integer.toString(y)+")";
	return rezultat;
	}


public float distance(int x, int y) {
	if (x>y) return x-y;
	else return y-x;
	}


//public double distance(int x, int y, int a, int b) {
//	return Math.sqrt((a-x)*(a-x)+(b-y)*(b-y));
//	}
public double distance(MyPoint nou) {
	return Math.sqrt((nou.x-x)*(nou.x-x)+(nou.y-y)*(nou.y-y));
	}
}



