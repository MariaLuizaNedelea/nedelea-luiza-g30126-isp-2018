package g30126.nedelea.luiza.l3.e5;

import becker.robots.*;

public class Ex5
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Thing parcel = new Thing(ny, 2, 2);
      
      Robot cartof = new Robot(ny, 1, 2, Direction.WEST);
      Wall blockAve0 = new Wall(ny, 1, 1, Direction.WEST);
      Wall blockAve01 = new Wall(ny, 2, 1, Direction.WEST);
      Wall blockAve1 = new Wall(ny, 1, 2, Direction.EAST);
      //Wall blockAve11 = new Wall(ny, 2, 2, Direction.EAST);
      Wall blockAve2 = new Wall(ny, 1, 2, Direction.NORTH);
      Wall blockAve21 = new Wall(ny, 1, 1, Direction.NORTH);
      Wall blockAve4 = new Wall(ny, 2, 1, Direction.SOUTH);
      Wall blockAve41 = new Wall(ny, 1, 2, Direction.SOUTH);
 
      
      // ann goes to meet mark
      cartof.move(); 
      cartof.turnLeft();
      
      cartof.move();
      cartof.turnLeft();
      cartof.move();
      cartof.pickThing();
      cartof.turnLeft();
      cartof.turnLeft();
      cartof.move(); 
      cartof.turnLeft(); 
      cartof.turnLeft(); 
      cartof.turnLeft(); 
      cartof.move();
      cartof.turnLeft(); 
      cartof.turnLeft(); 
      cartof.turnLeft(); 
      cartof.move(); 
          
   }
} 



