package g30126.nedelea.luiza.l3.e3;

import becker.robots.*;

public class Ex3
{
   public static void main(String[] args)
   {
      // Set up the initial situation
      City ny = new City();
      Robot cartof = new Robot(ny, 1, 1, Direction.NORTH);
 
      
      // ann goes to meet mark
      cartof.move(); 
      cartof.move();
      cartof.move();
      cartof.move();
      cartof.move();
      // se intoarce
      cartof.turnLeft();
      cartof.turnLeft();
      // finished turning right
      cartof.move();
      cartof.move();
      cartof.move();
      cartof.move();
      cartof.move();
     
   }
} 

