package g30126.nedelea.luiza.l8.e3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Ex3 {

	public static void Encript(String text) throws IOException {
		char[] sir=new char[text.length()];
		int k=0;
		for(int i=0;i<text.length();i++) {
			char c=text.charAt(i);
			int ascii = (int)c-1;
			sir[k++]=(char)ascii;
		}
		System.out.println(sir);
		File f = new File("text.enc");
		FileWriter fw= new FileWriter(f);
		fw.write(sir);
		fw.close();
		
	}
	
	public static void Decript(String text) throws IOException {
		char[] sir=new char[text.length()];
		int k=0;
		for(int i=0;i<text.length();i++) {
			char c=text.charAt(i);
			int ascii = (int)c+1;
			sir[k++]=(char)ascii;
		}
		System.out.println(sir);
		File f = new File("text.dec");
		FileWriter fw= new FileWriter(f);
		fw.write(sir);
		fw.close();
	}
	
	public static void main(String[] args) throws IOException {
		String nume;
		char raspuns = 0;
		Scanner scan = new Scanner(System.in);
		String line = null;
		String fraza = null;
		do {
	        System.out.println("Meniu");
	        System.out.println("a - Cititi din Fisier");
	        System.out.println("b - Encripted MODE");
	        System.out.println("c - Decripted MODE");
	        System.out.println("e - EXIT");
	        
	        String linie = scan.nextLine();
			raspuns =linie.charAt(0);
	        
	        switch(raspuns) {
	           case 'a': case 'A':
	        	 System.out.println("Introduceti numele fisierului: "); 
	        	 nume = scan.nextLine();
		BufferedReader in = new BufferedReader(new FileReader(nume));
		System.out.println("Sir citit din fisier: ");
		while((line = in.readLine()) != null)
		{
		    System.out.println(line);
		    fraza=line;
		    
		}
		in.close();
	    break;  
	       case 'b': case 'B':
	               System.out.println("Encripted Activated:");
	                  Encript(fraza);
	                  break;  
	       case 'c': case 'C':
	               System.out.println("Decripted Activated:");
	                  Decript(fraza); 
	        
	        }
	        }while(raspuns!='e' && raspuns!='E');
	        System.out.println("Program terminat.");
	        

	}

}
