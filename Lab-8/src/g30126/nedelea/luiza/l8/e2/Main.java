package g30126.nedelea.luiza.l8.e2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;



public class Main {

	public static void containsChar(String s, char caracter) {
	   int k=0;
		for(int i=0;i<s.length();i++) {
	    	if (s.charAt(i)==caracter) k++;
	    }
		System.out.println("Caracterul cautat este prezent de: ");
		System.out.println(k);
	}
	
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader("data.txt"));
		String line = null;
		String fraza = null;
		char caracter;
		System.out.println("Sir citit din fisier: ");
		while((line = in.readLine()) != null)
		{
		    System.out.println(line);
		    fraza=line;
		    
		}
		in.close();
		
		System.out.println("Introduceti caracterul de cautat");
		caracter = (char) System.in.read();
		containsChar(fraza,caracter);
	}

}
