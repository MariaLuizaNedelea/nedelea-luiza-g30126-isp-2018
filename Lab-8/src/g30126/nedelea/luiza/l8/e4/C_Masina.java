package g30126.nedelea.luiza.l8.e4;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class C_Masina implements  Serializable{
	ArrayList<Car> masini = new ArrayList<>();
	
	Car createCar(String model, double price){
        Car z = new Car(model, price);
        System.out.println(z+" - Masina CREATA.");
        return z;
  }
	
	public void addCar(Car c)
	 {
		masini.add(c);
	 }
	
	public void printCars() {
		Iterator<Car> i = masini.iterator();
        while(i.hasNext()){
        	Car masina_actuala = i.next();
        	System.out.println( masina_actuala);
        }
	}

}
