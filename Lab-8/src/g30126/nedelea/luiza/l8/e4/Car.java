package g30126.nedelea.luiza.l8.e4;

import java.io.Serializable;

public class Car implements  Serializable{
	private double price;
	private String model;
	public Car(String model, double price) {
		this.model=model;
		this.price=price;
	}
	public double getPrice() {
		return price;
	}
	public String getModel() {
		return model;
	}
	
	public String toString()
	{
	return "Car MODEL: "+model+"  & PRICE: "+price;	
	}
}
