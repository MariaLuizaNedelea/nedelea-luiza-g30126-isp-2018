package g30126.nedelea.luiza.l8.e4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		char raspuns;
		C_Masina masini = new C_Masina();
		do {
	        System.out.println("Meniu");
	        System.out.println("a - CREATI SI STOCATI MASINA");
	        System.out.println("b - AFISATI MASINA");
	        System.out.println("c - AFISATI TOATE MASINILE STOCATE");
	        System.out.println("e - EXIT");
	       
	        Scanner scan = new Scanner(System.in);
	        String linie = scan.nextLine();
	        raspuns =linie.charAt(0);
	        
	        switch(raspuns) {
	           case 'a': case 'A':
	        	   System.out.println("Introduceti numele fisierului unde va fi stocata masina: "); 
		           String nume1 = scan.nextLine();
	        	   FileOutputStream fileOutputStream = new FileOutputStream(nume1);
	        	   System.out.println("Introduceti modelul masinii: "); 
		           String MODEL = scan.nextLine();
		           System.out.println("Introduceti pretul masinii: "); 
		           double pret = scan.nextDouble();
	       		   Car car=masini.createCar(MODEL, pret);
	       		   masini.addCar(car);
	       		   //stocare fiecare fisier intr-un fisier separat
	        	   ObjectOutputStream output = new ObjectOutputStream(fileOutputStream);
	        	   output.writeObject(car);
	        	   output.close();
	        	   //stocare toate masinile din lista intr-un fisier
	        	   FileOutputStream fileOutputStream2 = new FileOutputStream("masini.txt");
	        	   ObjectOutputStream output2 = new ObjectOutputStream(fileOutputStream2);
	        	   output2.writeObject(masini);
	        	   output2.close();
	           break;
	           
	           case 'b': case 'B':
	        	   System.out.println("Introduceti numele fisierului de unde va fi afisata masina: "); 
		        	String nume2 = scan.nextLine();
		        	FileInputStream fileInputStream = new FileInputStream(new File(nume2));
		        	ObjectInputStream input = new ObjectInputStream(fileInputStream);
		        	Car car2 = (Car) input.readObject();
		        	System.out.println(car2.toString());
		        	input.close();
	           break; 
	           
	           case 'c': case 'C':
	        	   System.out.println("-- Masini existente --");
	        	   //masini.printCars();
	        	   FileInputStream fileInputStream2 = new FileInputStream(new File("masini.txt"));
		        	ObjectInputStream input2 = new ObjectInputStream(fileInputStream2);
		        	C_Masina car3 = (C_Masina) input2.readObject();
		        	car3.printCars();
		        	input2.close();
	           break;  
}
	        }while(raspuns!='e' && raspuns!='E');
	        System.out.println("Program terminat.");
	        

	}

}