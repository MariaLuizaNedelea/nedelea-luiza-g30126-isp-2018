package g30126.nedelea.luiza.l5.e3;
import java.util.*;

/**
 * 
 */
public abstract class Sensor {

 private String location;
    
 public Sensor() {
    location="Camera nr. 1";
    }

   abstract public int readValue();

    public String getLocation() {
        return "Location: "+location;
    }

}