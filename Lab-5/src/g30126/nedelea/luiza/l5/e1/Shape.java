package g30126.nedelea.luiza.l5.e1;

public abstract class Shape {
	protected String color;
	protected boolean filled;
		public Shape()
			 {
			filled=true;
			color="red";
			 }
		public Shape(String color, boolean filled)
				{ 
				this.filled=filled;
				this.color=color;
				}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public boolean isFilled() {
			return filled;
		}
		public void setFilled(boolean filled) {
			this.filled = filled;
		}
		
    public abstract String String();
	abstract double getArea();
	abstract double getPerimeter();
	

public static void main(String[]args)
{
	Shape[] sh=new Shape[3];
	sh[0]=new Circle(2.0,"blue",true);
	sh[1]=new Rectangle(2.0,3.0);
	sh[2]=new Square(2.0);
	for(int i=0;i<3;i++)
		{
		System.out.println("Aria "+sh[i].getArea());
		System.out.println("Perimetru "+sh[i].getPerimeter());
		}
}
}
