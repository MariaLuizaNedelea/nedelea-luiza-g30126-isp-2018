package g30126.nedelea.luiza.l5.e1;

 public class Rectangle extends Shape {
	protected double width;
	protected double length;
	
	public Rectangle()
	{
		width=1.0;
		length=1.0;
	}
	
	public Rectangle(double width,double length)
	{	
		this.length=length;
		this.width=width;
	}
	public Rectangle(double width, double length, String color, boolean filled)
	{	
		super(color,filled);
		this.length=length;
		this.width=width;
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	@Override
	 double getArea()
	{
		return width*length;
	}
	@Override
	 double getPerimeter()
	{
		return 2*(width+length);
	}
	
	@Override
	public String String() 
	{
	return "A rectangle with width = "+width+" and length = "+length+" which is a subclass of "+toString();
	}
	

}
