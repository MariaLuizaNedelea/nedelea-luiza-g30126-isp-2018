package g30126.nedelea.luiza.l5.e1;

public class Circle extends Shape {
	protected double radius;
	
	public Circle()
	{
		radius=1.0;
	}
	
	public Circle(double radius)
	{
		this.radius=radius;
	}
	
	public Circle(double radius, String color,boolean filled)
		{
		super(color,filled);
		this.radius=radius;
		}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	@Override
	 double getArea()
		{
		return 3.14*radius*radius;
		}
	@Override
	 double getPerimeter()
	{
		return 2*radius;
	}

	@Override
	public String String() 
	{
	return "A circle with radius = "+radius+" which is a subclass of "+toString();
	}
	
}
