package g30126.nedelea.luiza.l5.e2;

public class ProxyImage implements Image{
	 
	   private RealImage realImage;
	   private String fileName;
	   private int arg;
	 
	   public ProxyImage(String fileName){
	      this.fileName = fileName;
	   }
	   
	   public ProxyImage(String fileName, int arg)
	   		{this.fileName = fileName;
	   		this.arg=arg;
	   		}
	 
	   @Override
	   public void display() {
		   	      if(realImage == null){
	         realImage = new RealImage(fileName);
	      }
	      if(arg==1)
	      realImage.display();
	      else RotatedImage();
	   }
	   
	   @Override
	   public void RotatedImage() {
		   System.out.println("Display Rotated " + fileName);
	}
	}
