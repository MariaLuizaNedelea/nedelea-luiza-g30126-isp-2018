package g30126.nedelea.luiza.l5.e2;

public class TestImage {
	static void display(Image filename)
	{
		filename.display();
}
	static public void main(String[]args) {
		
	Image filename1 = new RealImage("Imagine 1"); 
	display(filename1);
	Image filename2 = new ProxyImage("Imagine 2",1); 
	display(filename2);
	}
}
