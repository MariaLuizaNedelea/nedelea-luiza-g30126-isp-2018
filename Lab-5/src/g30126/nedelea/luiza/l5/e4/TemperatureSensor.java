package g30126.nedelea.luiza.l5.e4;
import java.util.*;

/**
 * 
 */
public class TemperatureSensor extends Sensor {

    /**
     * Default constructor
     */
    public TemperatureSensor() {
    	super();
    }
    public int readValue() {
   	 Random r = new Random();
   	 int number=r.nextInt(101);
   	 return number;
   	 
    }

}